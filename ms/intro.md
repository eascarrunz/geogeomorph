#Introduction
Geometric morphometrics by means of landmark analysis have had considerable success in the description of subtle or complex shape attributes that are otherwise difficult to characterise. Thus, landmark data would seem an attractive source of information for phylogenetic inference in cases where the definition of discrete morphological characters is challenging. However, this use of these data has been historically limited for theoretical and empirical reasons.

Recently, @Catalano2010 and @Goloboff2011 proposed a method that extends the logic and algorithmic machinery of parsimony analysis to the analysis of landmark coordinates. The new approach has sparked considerable interest, although its usefulness seems limited. In a study of 41 empirical datasets @Catalano2016 showed that LAUP does not seem to perform significantly better than simple clustering methods.

Here we present a case study of the application of LAUP in resolving the phylogeny of geoemyidid turtles from shell characters. Geoemydidae is a large clade comprising around 21% of the currently recognised turtle species [@vanDijk2014]. In the last 20 years, a partial consensus of the evolutionary history of the group has emerged from molecular phylogenetic analyses [@Barth2004; @Feldman2004; @Spinks2004; @Le2007; @Le2008; @Sasaki2006; @Spinks2012]. In contrast, phylogenetic analyses based on morphological characters have yielded very inconsistent results [@Hirayama1985; @Yasukawa2001; @Joyce2004; @Garbin2017]. Indeed, efforts to place fossils in the phylogeny of the group have had to resort to using the molecular phylogenies as a backbone constraint for the morphomological data. Major problems with the morphological data seem to be the presence of extensive polymorphism and homoplasy, and the scarcity of discrete characters in the geoemydid shell. The use of landmarks is an interesting alternative to traditional discrete characters for at least two major reasons in this case:

1) Most of the traditional shell characters are positional, generally referring to the position of a suture or sulcus relative to another. This kind of phenotypic variation is continuous by nature. Being able to represent and analyse these changes as continuous allows to obtain a more fine-grained resolution from the same character, and avoids the problem of arbitrary binning the continuous variation into discrete states.

2) Landmarks allow to capture and represent some shell varaiation that is apparent to researchers, but difficult to characterise and to code into states. Examples of this variation include the degree of "doming" and the contour of the carapace.

Thus, the use of landmarks could help palliate the problem of the scarcity of discrete states.
#Methods
##Data acquisition

We sampled geoemydid specimens from the collections of the Field Museum of Natural History, Peter C.H. Pritchard's Chelonian Research Institute, the Peabody Museum of Natural History, and the Harvard Museum of Comparative Anatomy. Only bony shells without epidermal scutes were used, and we excluded juvenile specimens.

We landmarks corresponding to triple points in the mesh formed by the sutures of the bony plates of the shell, and in the mesh formed by the sulci left by the impression of the scutes on the bony carapace. Additionaly, type III landmarks were defined for the axillary and inguinal notches, and the posterior extremity of the xiphiplastron. This landmark scheme closely follows @Claude2004, with minor additions. In total, we digitised 152 carapace, and 48 plastron landmarks, which reduce to 77 and XX respectively in the analyses (see below). Landmark positions were digitised as 3D coordinates by means of a Microscribe. Most specimens were digitised twice in order to assess operator error.

##Data partitions

Instead of working with whole plastron configurations, we work separate configurations for the anterior and posterior lobes of each plastron. We did this to allow the inclusion of turtles with hinged plastra in the alignment.

LAUP conceptualises whole landmark configurations as analytical characters, therefore imposing an assumption of "independence".

##Terminal selection
We defined three sets of terminals for the analyses. The "species averages" (SA) set includes the average configuration of each separate species, comprising a total of 50 out of the 70 currently described. In the "genus averages" (GA) set, we used instead the average configuration of each genus. The reason for doing this is to reduce the tree space, facilitating the use of more thorough search and optimisation analyses. We do not acknowledge any biological significance to the taxonomical rank of clades. Our decision to average by genera is practical: a series of studies in the past fifteen years have revised the major geoemydid genera in detail [@Barth2004; @Feldman2004; @Spinks2004; @Le2007; @Le2008; @Spinks2012], so the current generic assignments can be considered monophyletic with high confidence.

The third set corresponds to the species averages of the six sampled members of the clade Cuora. This clade was chosen in order to examine in more detail the effect of the different search and optimisation parameters in the resolution of low-level interspecific relationships.
