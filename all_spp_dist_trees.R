library(geomorph)
source("gemo_fun.R")
library(ape)
library(phangorn)
# A function for bootstrapping
bs.array <- function (A, n) lapply(1L:n, function (x) A[sample(1L:dim(A)[1], dim(A)[1], replace = TRUE), , ])

# Load the GPAs of species-averaged configurations ####
ant_lobe <- readland.tps("sup/all_spp/ant_lobe_gpa.tps", specID = "ID")
pos_lobe <- readland.tps("sup/all_spp/pos_lobe_gpa.tps", specID = "ID")
carapace <- readland.tps("sup/all_spp/carapace_gpa.tps", specID = "ID")

# Find the "spp" common to all datasets. ####
# This is done to avoid the confounding effect of missing data in the analyses combining carapace and plastron.
spp_plastron <- dimnames(ant_lobe)[[3]] # ant_lobe and pos_lobe have the same spp
spp_carapace <- dimnames(carapace)[[3]]
spp_common <- intersect(spp_plastron, spp_carapace)

# Create the distance matrices for each dataset ####
d_ant_lobe <- dist(two.d.array(ant_lobe))
d_pos_lobe <- dist(two.d.array(pos_lobe))
d_carapace <- dist(two.d.array(carapace))
# Distances of combined datastets withhout rescaling
d_plastron <- dist(cbind(two.d.array(ant_lobe), two.d.array(pos_lobe)))
d_shell <- dist(cbind(two.d.array(ant_lobe[, , spp_common]), two.d.array(pos_lobe[, , spp_common]), two.d.array(carapace[, , spp_common])))
# Distances of combined datastets with rescaling by number of landmarks (p)
p_ant_lobe <- dim(ant_lobe)[1]
p_pos_lobe <- dim(pos_lobe)[1]
p_carapace <- dim(carapace)[1]
d_plastron_rsp <- dist(cbind(two.d.array(ant_lobe)/p_ant_lobe, two.d.array(pos_lobe)/p_pos_lobe))
d_shell_rsp <- dist(cbind(two.d.array(ant_lobe[, , spp_common])/p_ant_lobe, two.d.array(pos_lobe[, , spp_common])/p_pos_lobe, two.d.array(carapace[, , spp_common])/p_carapace))

d_mat <- list(d_ant_lobe, d_pos_lobe, d_carapace, d_plastron, d_shell, d_plastron_rsp, d_shell_rsp)

# Compute the NJ trees of each dataset ####
nj_trees <- lapply(d_mat, nj)
class(nj_trees) <- "multiPhylo"

nj_trees <- root(nj_trees, "Malaclemys terrapin", resolve.root = TRUE)
nj_trees <- reorder(nj_trees)
plot(nj_trees, cex = .5)

# Compute the UPGMA trees ####
library(phangorn)
upgma_trees <- lapply(d_mat, upgma)
class(upgma_trees) <- "multiPhylo"
plot(upgma_trees)

#### Bootstrap ####
 
ant_lobe_bs <- bs.array(ant_lobe, 500)
pos_lobe_bs <- bs.array(pos_lobe, 500)
carapace_bs <- bs.array(carapace, 500)

# Create the distance matrices for each dataset ####
d_ant_lobe <- lapply(ant_lobe_bs, function (x) dist(two.d.array(x)))
d_pos_lobe <- lapply(pos_lobe_bs, function (x) dist(two.d.array(x)))
d_carapace <- lapply(carapace_bs, function (x) dist(two.d.array(x)))
# Distances of combined datastets withhout rescaling
d_plastron <- lapply(1:500, function (x) dist(cbind(two.d.array(ant_lobe_bs[[x]]), two.d.array(pos_lobe_bs[[x]]))))
d_shell <- lapply(1:500, function (x) dist(cbind(two.d.array(ant_lobe_bs[[x]][, , spp_common]), two.d.array(pos_lobe_bs[[x]][, , spp_common]), two.d.array(carapace_bs[[x]][, , spp_common]))))
# Distances of combined datastets with rescaling by number of landmarks (p)
p_ant_lobe <- dim(ant_lobe)[1]
p_pos_lobe <- dim(pos_lobe)[1]
p_carapace <- dim(carapace)[1]
d_plastron_rsp <- lapply(1:500, function (x) dist(cbind(two.d.array(ant_lobe_bs[[x]])/p_ant_lobe, two.d.array(pos_lobe_bs[[x]])/p_pos_lobe)))
d_shell_rsp <- lapply(1:500, function (x) dist(cbind(two.d.array(ant_lobe_bs[[x]][, , spp_common])/p_ant_lobe, two.d.array(pos_lobe_bs[[x]][, , spp_common])/p_pos_lobe, two.d.array(carapace_bs[[x]][, , spp_common])/p_carapace)))

d_mat <- list(d_ant_lobe, d_pos_lobe, d_carapace, d_plastron, d_shell, d_plastron_rsp, d_shell_rsp)

# Compute the NJ trees of each dataset ####
nj_bs <- vector("list", length = length(length))
for (i in 1:length(d_mat)) {
  nj_bs[[i]] <- lapply(d_mat[[i]], nj)
  class(nj_bs[[i]]) <- "multiPhylo"
  nj_bs[[i]] <- root(nj_bs[[i]], "Malaclemys terrapin", resolve.root = TRUE)
  nj_bs[[i]] <- reorder(nj_bs[[i]])
}

nj_trees <- lapply(1:length(nj_trees), function (x) plotBS(nj_trees[[x]], nj_bs[[x]], p = 0))
class(nj_trees) <- "multiPhylo"
write.tree(nj_trees[[1]], "trees/nj/ant_lobe.tre")
write.tree(nj_trees[[2]], "trees/nj/pos_lobe.tre")
write.tree(nj_trees[[3]], "trees/nj/carapace.tre")
write.tree(nj_trees[[4]], "trees/nj/plastron.tre")
write.tree(nj_trees[[5]], "trees/nj/shell.tre")
write.tree(nj_trees[[6]], "trees/nj/plastron_rsp.tre")
write.tree(nj_trees[[7]], "trees/nj/shell_rsp.tre")