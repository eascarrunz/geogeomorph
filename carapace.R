library(geomorph)
library(abind)

carapace <- readland.tps("tps/carapace.tps", specID = "ID")

claude <- read.table("carapaceGEOEMYDIDAE.csv")

gpagen(na.omit(carapace))

nb_landmarks <- 152

exclude_land <- rep(F, 152)
exclude_specimen <- rep(F, 167)

for (i in 1:dim(carapace)[3]) {
  if (any(is.na(carapace[, , i]))) {
    exclude_specimen[i] <- T
  }
  for (j in 1:dim(carapace)[1]) {
    if (is.na(carapace[j, 1, i])) {
      exclude_land[j] <- T
    }
  }
}

#### Replicate error analysis ####

x <- names(carapace[1, 1, ])

specimens <- regexpr("PCHP\\d+", x, perl = T)

specimens <- unique(regmatches(x, specimens))

rm(x)

pairs <- vector("list", length(specimens))

pairs_si <- vector("list", length(specimens))

names(pairs) <- specimens

a_replicates <- carapace[, , grep(".\\$a$", names(carapace[1, 1, ]), perl = T)]

b_replicates <- carapace[, , grep(".\\$b$", names(carapace[1, 1, ]), perl = T)]

for (i in 1:length(pairs)) {
  name_a <- paste(names(pairs)[[i]], "$a", sep = "")
  name_b <- paste(names(pairs)[[i]], "$b", sep = "")
  if (name_b %in% names(b_replicates[1, 1, ])) {
    pairs[[i]] <- abind(a_replicates[, , name_a], b_replicates[, , name_b], along = 3)
  } else {
    pairs[[i]] <- a_replicates[, , name_a]
  }
}

pairs_si <- vector("list", length = length(pairs))
names(pairs_si) <- specimens

for (i in 1:length(pairs_si)) {
  try(
  pairs_si[[i]] <- gpagen(pairs[[i]])
  )
}

pairs_error <- vector("list", length = length(pairs_si))
names(pairs_error) <- specimens

for (i in 1:length(pairs_si)) {
  try({
  pairs_error[[i]]$coords <- abs(pairs_si[[i]]$coords[, , 1] - pairs_si[[i]]$coords[, , 2])
  pairs_error[[i]]$dist <- sqrt((pairs_si[[i]]$coords[, 1, 1] - pairs_si[[i]]$coords[, 1, 2])^2 + (pairs_si[[i]]$coords[, 2, 1] - pairs_si[[i]]$coords[, 2, 2])^2 + (pairs_si[[i]]$coords[, 3, 1] - pairs_si[[i]]$coords[, 3, 2])^2)
  pairs_error[[i]]$Csize <- abs(pairs_si[[i]]$Csize[1] - pairs_si[[i]]$Csize[2])
  plot(pairs_si[[i]]$coords[, 1:2, 1], asp = 1, type = "n")
  points(pairs_si[[i]]$coords[, 1:2, 1], asp = 1, col = "red")
  points(pairs_si[[i]]$coords[, 1:2, 2], asp = 1, col = "blue")
  })
}

land_error <- matrix(NA, nrow = nb_landmarks, ncol = length(pairs_error))

for (i in 1:length(pairs_error)) {
  try({
    land_error[, i] <- pairs_error[[i]]$dist
  })
}

barplot(rowMeans(land_error, na.rm = T))
hist(rowMeans(land_error, na.rm = T))
hist(colMeans(land_error, na.rm = T))
