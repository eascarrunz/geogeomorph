library(stringr)
library(StereoMorph)
library(geomorph)
# Some custom functions of mine
source("gemo_fun_new.R")
source("Rfunctions1.txt")
set.seed(7099)

# Load carapace data from external tps file
carapace <- readland.tps("tps/carapace.tps", specID = "ID")
# Load specimen details
specimens <- read.csv("specimens_definitive.csv", header = T, as.is = T)[, 5:10]

nb_landmarks <- dim(carapace)[1]
nb_replicates <- dim(carapace)[3]

# Load landmark definitions, which specify the laterality of each landmark.
landmark_definitions <- read.table("landmark_definitions.txt", header = T, as.is = T)

land_left <- landmark_definitions[landmark_definitions$side == "left", "landmark_number"]
land_right <- landmark_definitions[landmark_definitions$side == "right", "landmark_number"]
land_medial <- landmark_definitions[landmark_definitions$side == "medial", "landmark_number"]
keep_lands <- c(land_right, land_medial) # Thes are the landmarks that are going to be kept for further analyses.

replicate_match <- match(sub("\\$\\w", "", dimnames(carapace)[[3]]), specimens$call_code)

# Load the abnormality data.
abnormals <- read.csv("abn_fix.csv", as.is = TRUE)[, c("call_code", "dont_use")]
# Sort the abnormality data by the carapace array.
abnormals <- abnormals[replicate_match, ]

for (i in 1:nb_replicates) {
  abn <- sort(as.numeric(unlist(strsplit(abnormals[i, 2], ","))))
  M <- carapace[, , i]
  land_names <- vector(mode = "character", length = nrow(M))
  land_names[land_left] <- paste0(1:length(land_left), "_L")
  land_names[land_right] <- paste0(1:length(land_right), "_R")
  land_names[land_medial] <- paste0(1:length(land_medial), "_M")
  rownames(M) <- land_names
  M[abn, ] <- NA
  M2 <- reflectMissingLandmarks(M, average=TRUE)
  carapace[, , i] <- M2$lm.matrix[match(rownames(M), rownames(M2$lm.matrix)), ]
}

# Excluding specimens with any NA landmarks
carapace <- exclude_specimen(carapace)
# Calculate the average between replicates of specimens
carapace <- average_replicates(carapace)

# Fetching the "species" names ("lisu": least inclusive systematic unit, can also be subspecies) and maturity data by matching the call_code (specimen voucher numbers)
specimen_match <- match(dimnames(carapace)[[3]], specimens$call_code)
dimnames(carapace)[[3]] <- str_extract(specimens$lisu[specimen_match], "(\\S+)(\\s)(\\S+)") # This will merge all subspecies into their parent species.
maturity <- specimens$maturity[specimen_match]

# Exclude juveniles
maturity[which(maturity=="")] <- "adult" # Specimens without maturity data will be assumed to be adults
carapace <- keep_adults(carapace, maturity)

# Calculate species shape by averaging specimens
carapace <- average_species(carapace)

# Discarding some unidentified and hybrid specimens
carapace <- carapace[, , - which(dimnames(carapace)[[3]] == "Mauremys iversoni")] # Remove Mauremys iversoni because it is a hybrid between Cuora trifasciata and Mauremys mutica mutica
carapace <- carapace[, , - which(dimnames(carapace)[[3]] == "Cuora sp")] # Remove Cuora sp
carapace <- carapace[, , - which(dimnames(carapace)[[3]] == "Mauremys sp")] # Remove Mauremys sp
carapace <- carapace[, , - which(dimnames(carapace)[[3]] == "Geoemydidae sp")] # Remove unidentified specimen
carapace <- carapace[, , - which(dimnames(carapace)[[3]] == "Gopherus polyphemus")] # This species won't be used
# carapace <- carapace[, , - which(dimnames(carapace)[[3]] == "Malaclemys terrapin")] # This species won't be used
carapace <- carapace[, , - which(dimnames(carapace)[[3]] == "Glyptemys insculpta")] # This species won't be used
carapace <- carapace[, , - which(dimnames(carapace)[[3]] == "Graptemys barbouri")] # This species won't be used
carapace <- carapace[, , - which(dimnames(carapace)[[3]] == "Graptemys geographica")] # This species won't be used
carapace <- carapace[, , - which(dimnames(carapace)[[3]] == "Graptemys nigrinoda")] # This species won't be used
carapace <- carapace[, , - which(dimnames(carapace)[[3]] == "Graptemys versa")] # This species won't be used
carapace <- carapace[, , - which(dimnames(carapace)[[3]] == "Deirochelys reticularia")] # This species won't be used

# Write data of all configurations (right side only)
writeland.tps(carapace[keep_lands, , ], "sup/all_spp/carapace.tps")
carapace_gen <- average_genera(carapace)
dimnames(carapace_gen)[[3]] <- gsub(" \\w+", "", dimnames(carapace_gen)[[3]])

writeland.tps(carapace_gen[keep_lands, , ], "sup/genera/carapace.tps")

# Write GPA data of all configurations (right side only)
carapace_gpa <- gpagen(carapace)$coords
writeland.tps(round(carapace_gpa[keep_lands, , ], 3), "sup/all_spp/carapace_gpa.tps")
carapace_gpa_gen <- gpagen(carapace_gen)$coords
writeland.tps(round(carapace_gpa_gen[keep_lands, , ], 3), "sup/genera/carapace_gpa.tps")

# Do the same for the sulcus landmarks only
scute_lands <- which(landmark_definitions$layer == "scutes")
writeland.tps(carapace[intersect(keep_lands, scute_lands), , ], "sup/all_spp/carapace_scutes.tps")

# Do the same for the bone landmarks only
bone_lands <- which(landmark_definitions$layer == "bones")
writeland.tps(carapace[intersect(keep_lands, bone_lands), , ], "sup/all_spp/carapace_bones.tps")

# Write TNT matrices ####

# Create matrices with all the configurations
writeland.tnt(carapace[keep_lands, ,], "tnt_data/all_spp/carapace.tnt", log = NULL)
ant_lobe <- readland.tps("sup/all_spp/ant_lobe.tps", specID = "ID")
pos_lobe <- readland.tps("sup/all_spp/pos_lobe.tps", specID = "ID")
writeland.tnt(list(carapace = carapace[keep_lands, , ], ant_lobe = ant_lobe, pos_lobe = pos_lobe), "tnt_data/all_spp/shell.tnt", log = NULL)

# GPA carapace
writeland.tnt(carapace_gpa[keep_lands, ,], "tnt_data/all_spp/carapace_gpa.tnt", log = NULL)
writeland.tnt(carapace_gpa_gen[keep_lands, ,], "tnt_data/genera/carapace_gpa.tnt", log = NULL)

# Create matrices with all the GPA configurations
ant_lobe <- readland.tps("sup/all_spp/ant_lobe_gpa.tps", specID = "ID")
ant_lobe_gen <- readland.tps("sup/genera/ant_lobe_gpa.tps", specID = "ID")
pos_lobe <- readland.tps("sup/all_spp/pos_lobe_gpa.tps", specID = "ID")
pos_lobe_gen <- readland.tps("sup/genera/pos_lobe_gpa.tps", specID = "ID")

writeland.tnt(list(carapace = carapace_gpa[keep_lands, , ], ant_lobe = ant_lobe, pos_lobe = pos_lobe), "tnt_data/all_spp/shell_gpa.tnt", log = NULL)
writeland.tnt(list(carapace = carapace_gpa_gen[keep_lands, , ], ant_lobe = ant_lobe_gen, pos_lobe = pos_lobe_gen), "tnt_data/genera/shell_gpa.tnt", log = NULL)
